# rumba

rumba cleans up resources on Jetstream2. The rumba codebase implements [this issue](https://gitlab.com/jetstream-cloud/project-mgt/-/issues/16), please go there for context.

## How to run


### Environment and Dependencies

Set up a Python virtual environment and install dependencies:
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Setting up rumbarc

- Copy `rumbarc-example` to `rumbarc`
- Edit `rumbarc`, replace the placeholder values with actual values
- Run `export rumbarc` to set the variables in your shell

### Run the code

An example:

```
source rumbarc
python3 -m rumba
```

You will see help on usage if you don't pass any flags to the command.

## Code Formatting

This project uses [black](https://github.com/psf/black) to format code. After running the above commands, simply run `black your-changed-file-here` to apply formatting, or `black .` at the root of the project to format everything.

If you use an IDE, consider configuring it to format code every time you save a file. This makes life easier! [Here is how](https://black.readthedocs.io/en/stable/integrations/editors.html).

## `rumba_actions` table schema

```
mysql> CREATE TABLE `rumba_actions` (
->   `id` int NOT NULL AUTO_INCREMENT,
->   `charge_number` varchar(16) NOT NULL,
->   `resource` varchar(64) NOT NULL,
->   `timestamp` datetime NOT NULL,
->   `action` enum('notify_expired', 'notify_overdrawn', 'jta_enforce', 'disable', 'delete') NOT NULL,
->   `result` enum('success', 'failed') NOT NULL,
->   `error_message` text DEFAULT NULL,
->   PRIMARY KEY (`id`)
-> );
Query OK, 0 rows affected (0.02 sec)
```

## Test Project Setup/Teardown Code

To set up a test project:

```
python3 -m rumba.tests.test_project_setup
```

To tear down a test project:

```
python3 -m rumba.tests.test_project_teardown
```