import argparse
import logging
from typing import List

from rumba import actions
from rumba import db_getters
from rumba import helpers
from rumba.models import *


def process_allocations_for_charge_code(
    amie_conn, dry_run: bool, charge_code: str, allocations: List[Allocation]
):
    policy = helpers.get_policy()

    def get_is_overdrawn_(resource, overdraft_percent):
        return db_getters.get_is_overdrawn(
            amie_conn,
            charge_code,
            resource,
            overdraft_percent,
        )

    all_resources_actions = {r: None for r in [CPUResource, GPUResource, LMResource]}

    latest_resource_expiration_date = None

    # Determine which action to take for each resource
    for resource in all_resources_actions.keys():
        # If charge code has no allocation on this resource, do nothing for this resource.
        if resource not in list(map(lambda a: a.resource, allocations)):
            continue

        # Gather facts before decision logic

        expiration_date = db_getters.get_expiration_date(
            amie_conn, charge_code, resource
        )

        # Considering allocations to be expired at the end of the last day UTC
        expiration_datetime = helpers.get_latest_time_for_date(expiration_date)
        expired = helpers.is_at_least_days_since_event(expiration_datetime, 0)

        if expired and (
            not latest_resource_expiration_date
            or expiration_date > latest_resource_expiration_date
        ):
            # In case we send one email for multiple expired resources, determine the latest expiration date among them
            latest_resource_expiration_date = expiration_date

        if expired:
            expired_enough_to_disable = helpers.is_at_least_days_since_event(
                expiration_datetime, policy.disable_after_expiration_days
            )
            expired_enough_to_delete_email = helpers.is_at_least_days_since_event(
                expiration_datetime, policy.delete_email_after_expiration_days
            )

        most_recent_start_datetime = (
            db_getters.get_most_recent_allocation_start_datetime(
                amie_conn, charge_code, resource
            )
        )

        overdrawn = get_is_overdrawn_(resource, 0)
        overdrawn_enough_to_disable = get_is_overdrawn_(
            resource, policy.disable_after_overdraft_percent
        )

        last_expiration_email_datetime = db_getters.get_last_expiration_email_datetime(
            amie_conn, charge_code, resource
        )
        if last_expiration_email_datetime:
            enough_notice_to_disable_after_expiration_email = (
                helpers.is_at_least_days_since_event(
                    last_expiration_email_datetime,
                    policy.disable_after_expiration_email_days,
                )
            )

        last_delete_email_datetime = db_getters.get_last_delete_email_datetime(
            amie_conn, charge_code, resource
        )

        if last_delete_email_datetime:
            enough_notice_to_delete_after_delete_email = (
                helpers.is_at_least_days_since_event(
                    last_delete_email_datetime, policy.delete_after_delete_email_days
                )
            )

        last_overdraft_email_datetime = db_getters.get_last_overdraft_email_datetime(
            amie_conn, charge_code, resource
        )
        if last_overdraft_email_datetime:
            enough_notice_to_disable_after_overdraft_email = (
                helpers.is_at_least_days_since_event(
                    last_overdraft_email_datetime,
                    policy.disable_after_overdraft_email_days,
                )
            )

        last_disable_datetime = db_getters.get_last_disable_datetime(
            amie_conn, charge_code, resource
        )

        last_delete_datetime = db_getters.get_last_delete_datetime(
            amie_conn, charge_code, resource
        )

        # Done gathering facts, now decide what to do for this resource

        if expired and (
            (last_expiration_email_datetime is None)
            or last_expiration_email_datetime < expiration_datetime
        ):
            action = actions.notify_expired
        elif (
            overdrawn
            and (not expired)  # Because if allocation is expired, overdrawn is moot
            and (
                last_overdraft_email_datetime is None
                or last_overdraft_email_datetime < most_recent_start_datetime
            )
        ):
            action = actions.notify_overdrawn
        elif (
            (
                expired
                and expired_enough_to_disable
                and enough_notice_to_disable_after_expiration_email
            )
            or (
                overdrawn
                and overdrawn_enough_to_disable
                and enough_notice_to_disable_after_overdraft_email
            )
        ) and (
            last_disable_datetime is None
            or last_disable_datetime < most_recent_start_datetime
        ):
            action = actions.disable_allocation_resource
        elif (
            expired
            and expired_enough_to_delete_email
            and (
                (last_delete_email_datetime is None)
                or last_delete_email_datetime < expiration_datetime
            )
        ):
            action = actions.notify_delete
        elif (
            expired
            and (last_delete_email_datetime is not None)
            and enough_notice_to_delete_after_delete_email
            and (
                last_delete_datetime is None
                or last_delete_datetime < most_recent_start_datetime
            )
        ):
            action = actions.delete_allocation_resource
        else:
            action = None

        all_resources_actions[resource] = action

    if any(all_resources_actions.values()):
        logging.info(
            f"Taking these actions for charge code {charge_code}: {all_resources_actions}"
        )

    # Send only one expiration email for multiple resources, causing fewer notifications

    resources_notify_expired = list(
        r
        for r, value in all_resources_actions.items()
        if value == actions.notify_expired
    )

    if resources_notify_expired:
        actions.notify_expired(
            amie_conn,
            dry_run,
            resources_notify_expired,
            charge_code,
            latest_resource_expiration_date,
        )

    # Could use a DRY refactor with code above
    resources_notify_delete = list(
        r
        for r, value in all_resources_actions.items()
        if value == actions.notify_delete
    )

    if resources_notify_delete:
        actions.notify_delete(
            amie_conn,
            dry_run,
            resources_notify_delete,
            charge_code,
            latest_resource_expiration_date,
        )

    other_actions = dict(
        (resource, action)
        for resource, action in all_resources_actions.items()
        if action not in [actions.notify_expired, actions.notify_delete]
    )

    for resource, action in other_actions.items():
        if action is not None:
            try:
                action(amie_conn, dry_run, resource, charge_code)
            except NotImplementedError:
                logging.info(f"{action} is not implemented")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dry-run", action="store_true", help="Do not actually take actions"
    )

    args = parser.parse_args()
    amie_conn = helpers.get_amie_conn()
    allocations = db_getters.get_allocations(amie_conn)

    allocations_by_charge_code = helpers.group_allocations_by_charge_code(allocations)

    for charge_code, allocations_ in allocations_by_charge_code.items():
        process_allocations_for_charge_code(
            amie_conn, args.dry_run, charge_code, allocations_
        )
