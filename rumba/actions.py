import logging
from typing import List

from rumba import openstack_helpers
from rumba import db_getters
from rumba import db_setters
from rumba import helpers
from rumba import mail
from rumba.models import *

# Overall actions that operate on a combination of charge code and Resource


def notify_expired(
    amie_conn,
    dry_run: bool,
    resources: List[Jetstream2Resource],
    charge_code: str,
    expiration_date: datetime.date,
):
    subject = mail.expired_warn_subject(charge_code, resources)
    message = mail.expired_warn_message(
        charge_code, resources, expiration_date=expiration_date
    )
    email_address = db_getters.get_pi_email(amie_conn, charge_code)
    try:
        if not dry_run:
            mail.send_email(helpers.get_email_config(), email_address, subject, message)
        logging.info(
            f"Emailed PI {email_address} of charge code {charge_code} notifying that resource(s) {list(map(lambda r: r.__repr__(),resources))} are expired as of {expiration_date}"
        )
        result = "success"
        error = None
    except Exception as e:
        logging.error(e)
        result = "failed"
        error = e
    if not dry_run:
        for resource in resources:
            db_setters.record_action_in_db(
                amie_conn, charge_code, resource, "notify_expired", result, error
            )


def notify_overdrawn(
    amie_conn,
    dry_run: bool,
    resource: Jetstream2Resource,
    charge_code: str,
):
    subject = mail.overdrawn_warn_subject(charge_code, resource)
    message = mail.overdrawn_warn_message(charge_code, resource)
    email_address = db_getters.get_pi_email(amie_conn, charge_code)
    try:
        if not dry_run:
            mail.send_email(helpers.get_email_config(), email_address, subject, message)
        logging.info(
            f"Emailed PI {email_address} of charge code {charge_code} notifying that resource {resource.__repr__()} is overdrawn"
        )
        result = "success"
        error = None
    except Exception as e:
        logging.error(e)
        result = "failed"
        error = e
    if not dry_run:
        db_setters.record_action_in_db(
            amie_conn, charge_code, resource, "notify_overdrawn", result, error
        )


def notify_delete(
    amie_conn,
    dry_run: bool,
    resources: List[Jetstream2Resource],
    charge_code: str,
    expiration_date: datetime.date,
):
    subject = mail.delete_warn_subject(charge_code, resources)
    message = mail.delete_warn_message(
        charge_code, resources, expiration_date=expiration_date
    )
    email_address = db_getters.get_pi_email(amie_conn, charge_code)
    try:
        if not dry_run:
            mail.send_email(helpers.get_email_config(), email_address, subject, message)
        logging.info(
            f"Emailed PI {email_address} of charge code {charge_code} notifying that resource(s) {list(map(lambda r: r.__repr__(),resources))} will be deleted"
        )
        result = "success"
        error = None
    except Exception as e:
        logging.error(e)
        result = "failed"
        error = e
    if not dry_run:
        for resource in resources:
            db_setters.record_action_in_db(
                amie_conn, charge_code, resource, "notify_delete", result, error
            )


def disable_allocation_resource(
    amie_conn,
    dry_run: bool,
    resource: Jetstream2Resource,
    charge_code: str,
):
    try:
        conn = openstack_helpers.get_conn()
        project_uuid = openstack_helpers.get_project_uuid(conn, charge_code)
        servers = openstack_helpers.get_servers_for_project_resource(
            conn, project_uuid, resource
        )

        def delete_server_floating_ips(server):
            return openstack_helpers.delete_server_floating_ips(
                conn, dry_run, project_uuid, server
            )

        list(map(delete_server_floating_ips, servers))
        logging.info(
            "Deleted floating IPs for server: "
            + str([ip.floating_ip_address for ip in server.id])
        )

        def shelve_server(server):
            openstack_helpers.shelve_server(conn, server)

        # Shelve the servers

        if not dry_run:
            list(map(shelve_server, servers))
        logging.info("Shelved servers: " + str([s.id for s in servers]))

        result = "success"
        error = None

    except Exception as e:
        logging.error(e)
        result = "failed"
        error = e

    if not dry_run:
        db_setters.record_action_in_db(
            amie_conn, charge_code, resource, "disable", result, error
        )


def delete_allocation_resource(
    amie_conn,
    dry_run: bool,
    resource: Jetstream2Resource,
    charge_code: str,
):
    try:
        conn = openstack_helpers.get_conn()
        project_uuid = openstack_helpers.get_project_uuid(conn, charge_code)
        servers = openstack_helpers.get_servers_for_project_resource(
            conn, project_uuid, resource
        )

        def delete_server_floating_ips(server):
            return openstack_helpers.delete_server_floating_ips(
                conn, dry_run, project_uuid, server
            )

        list(map(delete_server_floating_ips, servers))

        def delete_server(server):
            openstack_helpers.delete_server(conn, dry_run, server)

        list(map(delete_server, servers))
        if servers:
            logging.info("Deleted servers: " + str([s.id for s in servers]))

        # todo if no more resources are active, also delete volumes, images, floating IPs, auto-allocated network, etc.

        result = "success"
        error = None
    except Exception as e:
        logging.error(e)
        result = "failed"
        error = e

    if not dry_run:
        db_setters.record_action_in_db(
            amie_conn, charge_code, resource, "delete", result, error
        )


# Granular sub-actions


def remove_project_from_flavor_group(
    dry_run: bool, charge_code: str, resource: Jetstream2Resource
):
    logging.debug(
        f"shelving instances for project {charge_code} and resource {resource}"
    )
    raise NotImplementedError


def shelve_instances_project_user(dry_run: bool, charge_code: str, user_uuid: str):
    logging.debug(f"shelving instances for project {charge_code} and user {user_uuid}")
    raise NotImplementedError


def delete_instances_project_resource(
    dry_run: bool, charge_code: str, resource: Jetstream2Resource
):
    logging.debug(
        f"deleting instances for project {charge_code} and resource {resource}"
    )
    raise NotImplementedError


def delete_instances_project_user(dry_run: bool, charge_code: str, user_uuid: str):
    logging.debug(f"deleting instances for project {charge_code} and user {user_uuid}")
    raise NotImplementedError


def delete_floating_ips_project(dry_run: bool, charge_code: str):
    logging.debug(f"deleting floating IPs for project {charge_code}")
    raise NotImplementedError


def delete_network_resources_project(dry_run: bool, charge_code: str):
    logging.debug(f"deleting network resources for project {charge_code}")
    raise NotImplementedError


def delete_volumes_project(dry_run: bool, charge_code: str):
    logging.debug(f"deleting volumes for project {charge_code}")
    raise NotImplementedError


def delete_images_project(dry_run: bool, charge_code: str):
    logging.debug(f"deleting images for project {charge_code}")
    raise NotImplementedError


def delete_file_shares_project(dry_run: bool, charge_code: str):
    logging.debug(f"deleting file shares for project {charge_code}")
    raise NotImplementedError


def delete_object_storage_project(dry_run: bool, charge_code: str):
    logging.debug(f"deleting object storage for project {charge_code}")
    raise NotImplementedError
