import json
import logging
from typing import Type, List, Optional

import pymysql

from rumba import exceptions
from rumba import helpers
from rumba.models import *


def get_allocations(amie_conn: pymysql.connections.Connection) -> List[Allocation]:
    sql = """
    SELECT
        charge_number,
        resource,
        start_date,
        end_date,
        pi_uuid,
        service_units_allocated
    FROM
        projects
    """
    with amie_conn.cursor() as cursor:
        cursor.execute(sql)
        result = cursor.fetchall()

    def to_structured_allocation(result_item):
        return Allocation(
            result_item.get("charge_number"),
            Jetstream2Resource.from_amie_string(result_item.get("resource")),
            result_item.get("start_date"),
            result_item.get("end_date"),
            uuid.UUID(result_item.get("pi_uuid")),
            result_item.get("service_units_allocated"),
        )

    def is_not_storage_resource(structured_allocation):
        return structured_allocation.resource != StorageResource

    structured_result = list(map(to_structured_allocation, result))

    # Not doing anything with storage allocations right now
    no_storage = list(filter(is_not_storage_resource, structured_result))

    return no_storage


def get_is_overdrawn(
    amie_conn: pymysql.connections.Connection,
    charge_code: str,
    resource: Type[Jetstream2Resource],
    allowed_overdraft_percent: int,
) -> bool:
    """Returns boolean value indicating whether a given allocation charge code is overdrawn for a given resource,
    allowing for a given overdraft percent
    """
    sql = """
    SELECT
        ifnull(sum(projects.service_units_allocated), 0) AS allocated,
        ifnull(sum(balances.used), 0) AS used 
    FROM
        projects 
    LEFT JOIN
        balances 
            ON projects.id = balances.project_record_id 
    WHERE
        charge_number = %s 
        AND projects.resource = %s
    GROUP BY
        start_date, end_date
    ORDER BY
        start_date
    DESC
    LIMIT 1
    """
    with amie_conn.cursor() as cursor:
        cursor.execute(sql, (charge_code, resource.to_amie_string()))
        result = cursor.fetchone()
    if not result:
        raise exceptions.AllocationNotFound
    else:
        if result.get("allocated") == 0:
            if result.get("used") == 0:
                # No SUs allocated and nothing used, so not overdrawn
                overdrawn = False
            else:
                # Resource was used with no SUs allocated, so it is overdrawn
                overdrawn = True
        else:
            used_allocated_ratio = result.get("used") / result.get("allocated")
            overdraft_percent = (used_allocated_ratio - 1) * 100
            overdrawn = overdraft_percent > allowed_overdraft_percent
        log_level = logging.debug
        log_level(
            f'Allocation {charge_code} for {resource.__repr__()} {"is" if overdrawn else "is not"} overdrawn (allocated {result.get("allocated")} SUs and used {result.get("used")} SUs, allowing {allowed_overdraft_percent}% overdraft)'
        )
        return overdrawn


def get_expiration_date(
    amie_conn: pymysql.connections.Connection,
    charge_code: str,
    resource: Type[Jetstream2Resource],
) -> datetime.date:
    """For a given charge code and resource, returns date on which the last allocation expires"""
    sql = """
    SELECT
        max(end_date) AS expiration_date 
    FROM
        projects 
    LEFT JOIN
        balances 
            ON projects.id = balances.project_record_id 
    WHERE
        charge_number = %s 
        AND projects.resource = %s
    """
    with amie_conn.cursor() as cursor:
        cursor.execute(sql, (charge_code, resource.to_amie_string()))
        result = cursor.fetchone()
    if not result:
        raise exceptions.AllocationNotFound
    else:
        expiration_date = result.get("expiration_date")
        logging.debug(
            f"Allocation {charge_code} for {resource.__repr__()} expires on {expiration_date}"
        )
        return expiration_date


def get_last_expiration_email_datetime(
    amie_conn, charge_code: str, resource: Jetstream2Resource
) -> Optional[datetime.datetime]:
    return get_last_action_datetime_(amie_conn, charge_code, resource, "notify_expired")


def get_last_overdraft_email_datetime(
    amie_conn, charge_code: str, resource: Jetstream2Resource
) -> Optional[datetime.datetime]:
    return get_last_action_datetime_(
        amie_conn, charge_code, resource, "notify_overdrawn"
    )


def get_last_delete_email_datetime(
    amie_conn, charge_code: str, resource: Jetstream2Resource
) -> Optional[datetime.datetime]:
    return get_last_action_datetime_(amie_conn, charge_code, resource, "notify_delete")


def get_last_disable_datetime(
    amie_conn, charge_code: str, resource: Jetstream2Resource
) -> Optional[datetime.datetime]:
    return get_last_action_datetime_(amie_conn, charge_code, resource, "disable")


def get_last_delete_datetime(
    amie_conn, charge_code: str, resource: Jetstream2Resource
) -> Optional[datetime.datetime]:
    return get_last_action_datetime_(amie_conn, charge_code, resource, "delete")


def get_last_action_datetime_(
    amie_conn, charge_code: str, resource: Jetstream2Resource, action_str: str
) -> Optional[datetime.datetime]:
    sql = f"""
        SELECT
            MAX(timestamp) AS timestamp
        FROM
            rumba_actions
        WHERE
            charge_number = "{charge_code}"
            AND resource = "{resource.to_amie_string()}"
            AND action = "{action_str}"
            AND result = "success"
        """
    with amie_conn.cursor() as cursor:
        cursor.execute(sql)
    result = cursor.fetchone().get("timestamp")
    if not result:
        return None
    return result.astimezone(datetime.timezone.utc)


def get_most_recent_allocation_start_datetime(
    amie_conn, charge_code: str, resource: Jetstream2Resource
) -> datetime.datetime:
    sql = f"""
    SELECT
        MAX(start_date) AS start_date
    FROM
        projects
    WHERE
        charge_number = "{charge_code}"
        AND resource = "{resource.to_amie_string()}"
    """
    with amie_conn.cursor() as cursor:
        cursor.execute(sql)
    result = cursor.fetchone()
    start_date = result.get("start_date")
    start_datetime = helpers.get_latest_time_for_date(start_date)
    return start_datetime


def get_pi_email(amie_conn, charge_code: str):
    sql = f"""
    SELECT
        keystone.user.extra AS json
    FROM
        amie.projects
    LEFT JOIN
        keystone.user
    ON
        amie.projects.pi_uuid = keystone.user.id
    WHERE
        charge_number = "{charge_code}"
    ORDER BY
        start_date DESC
    LIMIT 1;
    """
    with amie_conn.cursor() as cursor:
        cursor.execute(sql)
    result = cursor.fetchone()
    result_json_str = result.get("json")
    result_json = json.loads(result_json_str)
    email = result_json.get("email")
    return email
