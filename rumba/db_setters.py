from typing import Literal, Optional

from rumba import helpers
from rumba.models import *


def record_action_in_db(
    amie_conn,
    charge_code: str,
    resource: Jetstream2Resource,
    action: Literal[
        "notify_expired",
        "notify_overdrawn",
        "notify_delete",
        "jta_enforce",
        "disable",
        "delete",
    ],
    result: Literal["success", "failed"],
    error_message: Optional[str],
):
    sql = """
    INSERT INTO
        amie.rumba_actions
        (charge_number, resource, timestamp, action, result, error_message)
    VALUES
        (%s, %s, %s, %s, %s, %s)
    """
    values = (
        charge_code,
        resource.to_amie_string(),
        helpers.get_now_utc(),
        action,
        result,
        error_message,
    )
    with amie_conn.cursor() as cursor:
        cursor.execute(
            sql,
            values,
        )
    amie_conn.commit()
