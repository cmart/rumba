class AllocationNotFound(Exception):
    pass


class ServerTimeout(Exception):
    pass
