import datetime
import os
from typing import Dict, List

import pymysql
from sqlalchemy.engine.url import make_url

from rumba import models

# Basic helpers that depend only on standard library and installed packages


def get_env_var(env_var_name):
    val = os.environ.get(env_var_name)
    if val is not None:
        return val
    else:
        raise KeyError("Environment variable {} is not defined".format(env_var_name))


def get_now_utc() -> datetime.datetime:
    # https://blog.ganssle.io/articles/2019/11/utcnow.html
    return datetime.datetime.now(tz=datetime.timezone.utc)


def is_at_least_days_since_event(event: datetime.datetime, days: int) -> bool:
    return event + datetime.timedelta(days=days) < get_now_utc()


def get_latest_time_for_date(date: datetime.date):
    latest_time = datetime.time(
        hour=23,
        minute=59,
        second=59,
        microsecond=999999,
        tzinfo=datetime.timezone.utc,
    )
    return datetime.datetime.combine(date, latest_time, tzinfo=datetime.timezone.utc)


# More advanced helpers. These may operate on structured types, but do not directly interact with database


def db_conn_dict_from_url(url):
    parsed_url = make_url(url)
    db_conn_dict = pymysql.connect(
        host=parsed_url.host,
        user=parsed_url.username,
        password=parsed_url.password,
        database=parsed_url.database,
        cursorclass=pymysql.cursors.DictCursor,
    )
    return db_conn_dict


def get_amie_conn():
    url = get_env_var("AMIE_DB_URL")
    return db_conn_dict_from_url(url)


def get_email_config() -> models.EmailConfig:
    return models.EmailConfig(
        get_env_var("SMTP_HOST"),
        get_env_var("SMTP_USERNAME"),
        get_env_var("SMTP_PASSWORD"),
        get_env_var("SMTP_FROM_ADDRESS"),
    )


def get_policy():
    def get_env_var_int(var_name):
        return int(get_env_var(var_name))

    return models.Policy(
        get_env_var_int("DISABLE_AFTER_EXPIRATION_DAYS"),
        get_env_var_int("DISABLE_AFTER_EXPIRATION_EMAIL_DAYS"),
        get_env_var_int("DELETE_EMAIL_AFTER_EXPIRATION_DAYS"),
        get_env_var_int("DELETE_AFTER_DELETE_EMAIL_DAYS"),
        get_env_var_int("DISABLE_AFTER_OVERDRAFT_PERCENT"),
        get_env_var_int("DISABLE_AFTER_OVERDRAFT_EMAIL_DAYS"),
    )


def group_allocations_by_charge_code(
    allocations: List[models.Allocation],
) -> Dict[str, List[models.Allocation]]:
    unique_charge_codes = set(map((lambda a: a.charge_code), allocations))

    def allocations_for_charge_code(charge_code):
        return list(filter(lambda a: a.charge_code == charge_code, allocations))

    d = dict()

    for charge_code in unique_charge_codes:
        d[charge_code] = allocations_for_charge_code(charge_code)

    return d


def resources_repr(resources: List[models.Jetstream2Resource]) -> str:
    resources_pluralization = "s" if len(resources) > 1 else ""
    resources_strs = map(lambda r: r.__repr__(), resources)
    resources_str = ", ".join(resources_strs)
    return resources_str + " resource" + resources_pluralization
