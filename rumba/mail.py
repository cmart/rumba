import datetime
from email.message import EmailMessage
import smtplib
from typing import List

from rumba import helpers
from rumba import models


# From https://docs.python.org/3/library/email.examples.html


def send_email(
    email_config: models.EmailConfig, email_to: str, subject: str, message: str
):
    message = message

    msg = EmailMessage()
    msg.set_content(message)

    msg["Subject"] = subject
    msg["From"] = email_config.smtp_from_address
    msg["To"] = email_to

    s = smtplib.SMTP_SSL(email_config.smtp_host)
    s.login(email_config.smtp_username, email_config.smtp_password)
    s.send_message(msg)
    s.quit()


def expired_warn_subject(
    charge_code: str,
    resources: List[models.Jetstream2Resource],
) -> str:
    return f"Jetstream2 allocation {charge_code} is expired for {helpers.resources_repr(resources)}"


def expired_warn_message(
    charge_code: str,
    resources: List[models.Jetstream2Resource],
    expiration_date: datetime.date,
) -> str:
    return f"""Dear Principal Investigator,

Your allocation {charge_code} for {helpers.resources_repr(resources)} on Jetstream2 expired on {expiration_date.isoformat()}. Your Jetstream2 instances and data will be disabled 10 days after expiration, and they are subject to deletion 30 days after expiration.

If you are still using Jetstream2, please renew your ACCESS allocation at https://allocations.access-ci.org .

You can read our policy for expired allocations at https://docs.jetstream-cloud.org/general/policies/#allocation-related-policies .

If you have questions, please email help@jetstream-cloud.org .

Regards,
Jetstream2 Support
"""


def overdrawn_warn_subject(
    charge_code: str,
    resource: models.Jetstream2Resource,
) -> str:
    return f"Jetstream2 allocation {charge_code} is overdrawn for {helpers.resources_repr([resource])}"


def overdrawn_warn_message(
    charge_code: str,
    resource: models.Jetstream2Resource,
) -> str:
    return f"""Dear Principal Investigator,

Your allocation {charge_code} for {helpers.resources_repr([resource])} on Jetstream2 has overdrawn its allocated service units (SUs). Your Jetstream2 instances and other resources will be disabled.

If you are still using Jetstream2, please transfer additional credits from your ACCESS allocation (if available), pursue a larger allocation size, or request a supplement at https://allocations.access-ci.org/requests .

If you have questions, please email help@jetstream-cloud.org .

Regards,
Jetstream2 Support
"""


def delete_warn_subject(
    charge_code: str,
    resource: models.Jetstream2Resource,
) -> str:
    return f"Jetstream2 will delete instances and data for allocation {charge_code}, {helpers.resources_repr([resource])}"


def delete_warn_message(
    charge_code: str,
    resources: List[models.Jetstream2Resource],
    expiration_date: datetime.date,
) -> str:
    return f"""Dear Principal Investigator,

Your allocation {charge_code} for {helpers.resources_repr(resources)} on Jetstream2 expired on {expiration_date.isoformat()}. Your Jetstream2 instances and data are subject to deletion 30 days after expiration. This is your final warning before we delete your instances and data.

If you are still using Jetstream2, please renew your ACCESS allocation at https://allocations.access-ci.org .

You can read our policy for expired allocations at https://docs.jetstream-cloud.org/general/policies/#allocation-related-policies .

If you have questions, please email help@jetstream-cloud.org .

Regards,
Jetstream2 Support
"""
