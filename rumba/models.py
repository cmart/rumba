from abc import ABCMeta, abstractmethod
import datetime
from typing import NamedTuple
import uuid


class Jetstream2Resource(ABCMeta):
    @classmethod
    @abstractmethod
    def to_amie_string(cls):
        pass

    @classmethod
    @abstractmethod
    def flavor_prefix(cls):
        pass

    @classmethod
    @abstractmethod
    def __repr__(self):
        return "Jetstream2 Resource"

    @classmethod
    def from_amie_string(cls, s):
        if s == "jetstream2.indiana.xsede.org":
            return CPUResource
        elif s == "jetstream2-gpu.indiana.xsede.org":
            return GPUResource
        elif s == "jetstream2-lm.indiana.xsede.org":
            return LMResource
        elif s == "jetstream2-storage.indiana.xsede.org":
            return StorageResource
        else:
            raise UnrecognizedResource(f"{s}")


class CPUResource(Jetstream2Resource):
    @classmethod
    def to_amie_string(cls):
        return "jetstream2.indiana.xsede.org"

    @classmethod
    @abstractmethod
    def flavor_prefix(cls):
        return "m"

    @classmethod
    def __repr__(self):
        return "CPU"


class GPUResource(Jetstream2Resource):
    @classmethod
    def to_amie_string(cls):
        return "jetstream2-gpu.indiana.xsede.org"

    @classmethod
    @abstractmethod
    def flavor_prefix(cls):
        return "g"

    @classmethod
    def __repr__(self):
        return "GPU"


class LMResource(Jetstream2Resource):
    @classmethod
    def to_amie_string(cls):
        return "jetstream2-lm.indiana.xsede.org"

    @classmethod
    @abstractmethod
    def flavor_prefix(cls):
        return "r"

    @classmethod
    def __repr__(self):
        return "Large Memory"


class StorageResource(Jetstream2Resource):
    @classmethod
    def to_amie_string(cls):
        return "jetstream2-storage.indiana.xsede.org"

    @classmethod
    @abstractmethod
    def flavor_prefix(cls):
        raise AttributeError

    @classmethod
    def __repr__(self):
        return "Storage"


class UnrecognizedResource(Exception):
    pass


class Allocation(NamedTuple):
    charge_code: str
    resource: Jetstream2Resource
    start_date: datetime.date
    end_date: datetime.date
    pi_uuid: uuid.UUID
    service_units_allocated: float


class Policy(NamedTuple):
    disable_after_expiration_days: int
    disable_after_expiration_email_days: int
    delete_email_after_expiration_days: int
    delete_after_delete_email_days: int
    disable_after_overdraft_percent: int
    disable_after_overdraft_email_days: int


class EmailConfig(NamedTuple):
    smtp_host: str
    smtp_username: str
    smtp_password: str
    smtp_from_address: str
