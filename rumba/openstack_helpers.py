import logging
import os
import time

import openstack.compute.v2.server

from rumba import exceptions
from rumba import models

openstack.enable_logging(debug=False)

# Keystone


def get_conn():
    return openstack.connect(
        auth_url=os.environ.get("OS_AUTH_URL"),
        project=os.environ.get("OS_PROJECT_NAME"),
        username=os.environ.get("OS_USERNAME"),
        password=os.environ.get("OS_PASSWORD"),
        region_name=os.environ.get("OS_REGION_NAME"),
        user_domain_name=os.environ.get("OS_USER_DOMAIN_NAME"),
        project_domain_name=os.environ.get("OS_PROJECT_DOMAIN_NAME"),
    )


def get_project_uuid(conn, project_name):
    return conn.identity.find_project(project_name).id


# Neutron


def delete_all_project_floating_ips(conn, project_uuid):
    floating_ips = conn.network.ips(project_id=project_uuid)

    for ip in floating_ips:
        conn.delete_floating_ip(ip)


# Nova


def get_flavors_for_resource(conn, resource: models.Jetstream2Resource):
    flavor_prefix = resource.flavor_prefix()

    flavors = list(conn.compute.flavors())

    return [f for f in flavors if f.name.startswith(flavor_prefix)]


def get_servers_for_project_resource(
    conn, project_uuid, resource: models.Jetstream2Resource
):
    flavors = get_flavors_for_resource(conn, resource)
    flavor_names = [f.name for f in flavors]

    servers = list(conn.compute.servers(all_projects=True, project_id=project_uuid))

    return [s for s in servers if s.flavor.name in flavor_names]


def wait_for_active_server(conn, server, timeout_sec=300, sleep_increment_sec=15):
    remaining_timeout_sec = timeout_sec
    while (
        conn.compute.get_server(server).status != "ACTIVE" and remaining_timeout_sec > 0
    ):
        remaining_timeout_sec -= sleep_increment_sec
        time.sleep(sleep_increment_sec)
    if conn.compute.get_server(server).status == "ACTIVE":
        return
    else:
        raise exceptions.ServerTimeout


def delete_server(conn, dry_run, s):
    if not dry_run:
        conn.compute.delete_server(s, ignore_missing=True, force=True)


def shelve_server(conn, s):
    # TODO decide what to do if we can't shelve a server. Probably log an error and move on.

    if s.status == "ACTIVE":
        conn.compute.shelve_server(s)
    elif s.status == "BUILD":
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "DELETED":
        # Nothing to do
        pass
    elif s.status == "ERROR":
        # TODO log an error
        pass
    elif s.status == "HARD_REBOOT":
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "MIGRATING":
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "PASSWORD":
        # Guessing, unsure if this will work
        conn.compute.shelve_server(s)
    elif s.status == "PAUSED":
        conn.compute.shelve_server(s)
    elif s.status == "REBOOT":
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "REBUILD":
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "RESCUE":
        conn.compute.unrescue_server(s)
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "RESIZE":
        wait_for_active_server(conn, s)
        conn.compute.confirm_server_resize(s)
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "REVERT_RESIZE":
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)
    elif s.status == "SHELVED":
        # Nothing to do
        pass
    elif s.status == "SHELVED_OFFLOADED":
        # Nothing to do
        pass
    elif s.status == "SHUTOFF":
        conn.compute.shelve_server(s)
    elif s.status == "SOFT_DELETED":
        # Nothing to do
        pass
    elif s.status == "STOPPED":
        conn.compute.shelve_server(s)
    elif s.status == "SUSPENDED":
        conn.compute.shelve_server(s)
    elif s.status == "UNKNOWN":
        # TODO decide what to do here
        pass
    elif s.status == "VERIFY_RESIZE":
        conn.compute.revert_server_resize(s)
        wait_for_active_server(conn, s)
        conn.compute.shelve_server(s)


def shelve_servers(conn):
    # TODO remove this function if we aren't using it
    # TODO do this stuff asynchronously for each server, because some tasks might take a while. use async/await.
    for s in conn.compute.servers():
        shelve_server(conn, s)


# Multi-service


def get_server_floating_ips(conn, project_uuid, server_uuid):
    ips = list(conn.network.ips(project_id=project_uuid))

    def is_ip_for_server(ip):
        try:
            device_id = ip.get("port_details").get("device_id")
            return server_uuid == device_id
        except AttributeError:
            # IP is not associated with a port
            return False

    return list(filter(is_ip_for_server, ips))


def delete_server_floating_ips(conn, dry_run, project_uuid, server):
    ips = get_server_floating_ips(conn, project_uuid, server.id)

    if not dry_run:
        list(map(conn.network.delete_ip, ips))

    if ips:
        logging.info(
            "Deleted floating IPs: " + str([ip.floating_ip_address for ip in ips])
        )
