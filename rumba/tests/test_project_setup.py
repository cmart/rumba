import openstack
from rumba.openstack_helpers import wait_for_active_server

openstack.enable_logging(debug=False)
conn = openstack.connect(cloud="jetstream2")

network = conn.network.get_auto_allocated_topology()

images = conn.image.images(visibility="public")

first_image = list(images)[0]


def create_server(name):
    return conn.compute.create_server(
        name=name,
        flavor_id=1,
        networks=[{"uuid": network.id}],
        image_id=first_image.id,
    )


def create_floating_ip():
    return conn.network.create_ip(network="public")


active_server = create_server("active")
active_server_floating_ip = create_floating_ip()
conn.compute.add_floating_ip_to_server(active_server, active_server_floating_ip)

server_to_pause = create_server("paused")
paused_server_floating_ip = create_floating_ip()
wait_for_active_server(conn, server_to_pause)
conn.compute.add_floating_ip_to_server(server_to_pause, paused_server_floating_ip)
conn.compute.pause_server(server_to_pause)

server_to_resize = create_server("resized_not_verified")
wait_for_active_server(conn, server_to_resize)
conn.compute.resize_server(server_to_resize, flavor=2)

server_to_stop = create_server("stopped")
wait_for_active_server(conn, server_to_stop)
conn.compute.stop_server(server_to_stop)

server_to_suspend = create_server("suspended")
wait_for_active_server(conn, server_to_suspend)
conn.compute.suspend_server(server_to_suspend)

# Non-admin user is not allowed to perform a reset state
# server_to_error = create_server("error")
# wait_for_active_server(conn, server_to_error)
# conn.compute.reset_server_state(server_to_error, "error")
