import openstack

openstack.enable_logging(debug=False)
conn = openstack.connect(cloud="jetstream2")

for s in conn.compute.servers():
    conn.compute.delete_server(s)

conn.network.delete_auto_allocated_topology()

for i in conn.network.ips():
    conn.network.delete_ip(i)
